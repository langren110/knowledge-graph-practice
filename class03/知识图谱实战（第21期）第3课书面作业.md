# 知识图谱实战（第21期）第3课书面作业
学号：115688

**作业内容：**  
1. 知识图谱实战：构建一个简单的知识图谱，内容可以自定，完成时间：3周

   要求：

   （1） 实体类型有两种或以上，关系有3种或以上

   （2）可以实现简单的问题查询


## 第1题
### 1.1 选题

构建一个以电影为主题的简单知识图谱：

* 有三个实体：电影、人（导演，演员）、风格
* 有三种关系：导演、演出、表现（即电影表现出什么风格）



### 1.2 数据获取

通过爬虫从豆瓣电影网站上获取电影相关信息。

采用scrapy框架实现

关键源代码如下。

爬虫代码movie.py

```python
class MovieSpider(scrapy.Spider):
    name = 'movie'
    allowed_domains = ['movie.douban.com']
    start_urls = ['http://movie.douban.com/']

    headers = {
        "Host": "movie.douban.com",
        "Referer": "https://movie.douban.com/top250?start=225&filter=",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-User": '?1',
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
    }

    def start_requests(self):
        for page in range(0, 11):
            page = page * 25
            url = "https://movie.douban.com/top250?start={0}&filter=".format(page)
            yield scrapy.Request(url, headers=self.headers, callback=self.movie_url_list_parse)

    def movie_url_list_parse(self, response):
        print('正在抓取:{0}'.format(response.url))
        url_results = []
        url_lists = response.xpath('//ol[@class="grid_view"]/li')
        for url_info in url_lists:
            url_detail = url_info.xpath('div[@class="item"]/div[@class="info"]/div[@class="hd"]/a/@href').extract()
            url_detail = "".join(url_detail).strip()
            url_results.append(url_detail)

            yield scrapy.Request(url_detail, headers=self.headers, callback=self.detail_parse)

    def detail_parse(self, response):
        movie_id = re.findall(r'(\d+)', response.url)
        movie_id = ''.join(movie_id)
        # 电影名称
        title = response.xpath('//*[@id="content"]/h1/span[1]/text()').extract()
        movie_name = "".join(title).strip()
        # 导演
        director = response.xpath('//*[@id="info"]/span[1]/span[2]/a/text()').extract()
        director = "".join(director)
        # 演员
        actor, movie_type = self.get_actor(response.url)
        actor = "".join(actor).strip()
        # 电影类型

        item = DoubanItem()
        # 电影ID
        item['movie_id'] = movie_id
        # 电影名称
        item['movie_name'] = movie_name
        # 导演
        item['movie_director'] = director
        # 演员
        item['movie_actor'] = actor
        # 电影类型
        item['movie_type'] = movie_type

        yield item

    def get_actor(self, url):
        response = requests.get(url, headers=self.headers)
        response.encoding = "utf-8"
        soup = BeautifulSoup(response.content, 'lxml')
        x = soup.find_all('div', id='info')[0]
        try:
            actor = x.find_all('span', class_="actor")[0].find_all('span', class_="attrs")
            movie_type = x.find_all('span', property='v:genre')
            movie_type_result = []
            for info in movie_type:
                info = info.text
                movie_type_result.append(info)
            movie_type_result = '/'.join(movie_type_result)
            for info in actor:
                actor_result = info.text
                return actor_result, movie_type_result
        except Exception as e:
            print(e)
```

items.py:

```python
import scrapy


class DoubanItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # 电影ID
    movie_id = scrapy.Field()
    # 电影名称
    movie_name = scrapy.Field()
    # 导演
    movie_director = scrapy.Field()
    # 演员
    movie_actor = scrapy.Field()
    # 电影类型
    movie_type = scrapy.Field()

```

pipelines.py:

```python
import pandas as pd


class DoubanPipeline(object):
    def __init__(self):
        self.results_all = []

    def process_item(self, item, spider):
        result = [item['movie_name'], item['movie_director'], item['movie_actor'], item['movie_type']]
        self.results_all.append(result)
        return item

    def close_spider(self, spider):
        try:
            df = pd.DataFrame(data=self.results_all, columns=['电影名称', '导演', '演员', '类型'])
            df.to_csv('douban.csv', index=False, encoding="utf-8_sig")
        except Exception as e:
            print(e)
```

执行效果：

![kgclass03-1](C:\Users\kzcao\gitee\知识图谱实战\class03\kgclass03-1.png)

### 1.3 图数据库建立

使用py2neo库来根据上一步骤采集的数据来建立图数据库。

代码如下：

```python
import pandas as pd
from py2neo import Node, Relationship, Graph, NodeMatcher, RelationshipMatcher
# 打开数据库
graph = Graph("http://localhost:7474/", auth=("neo4j", "fortest"))

# 建立一个节点
def create_node(graph, label, attrs):
    n = "_.name=" + "\"" + attrs["name"] + "\""
    matcher = NodeMatcher(graph)
    # 查询是否已经存在，若存在则返回节点，否则返回None
    value = matcher.match(label).where(n).first()
    # 如果要创建的节点不存在则创建
    if value is None:
        node = Node(label, **attrs)
        n = graph.create(node)
        return n
    return None

# 建立两个节点之间的关系
def create_relationship(graph, label1, attrs1, label2, attrs2, r_name):
    value1 = match_node(graph, label1, attrs1)
    value2 = match_node(graph, label2, attrs2)
    if value1 is None or value2 is None:
        return False
    r = Relationship(value1, r_name, value2)
    graph.create(r)


# 查询节点
def match_node(graph, label, attrs):
    n = "_.name=" + "\"" + attrs["name"] + "\""
    matcher = NodeMatcher(graph)
    return matcher.match(label).where(n).first()

df_movies = pd.read_csv('douban.csv')

for i in range(df_movies.shape[0]):
    movie = Node("Movie",name=df_movies.iat[i,0])
#     node_m = create_node(graph, 'Movie',{'name':movie})
    director = Node("Person",name=df_movies.iat[i,1], role='director')
#     node_d = create_node(graph, 'Person',{'name':director,'role':'director'})
    d = Relationship(director, "directed", movie)
    graph.create(d)
    actors = df_movies.iat[i,2].split('/')
    for actor in actors:
        actor = actor.strip()
        node_a = Node("Person", name=actor, role='actor')
        r = Relationship(node_a, "acted", movie)
        graph.create(r)
    styles = df_movies.iat[i,3].split('/')
    for style in styles:
        style = style.strip()
        node_s = Node("Style", name=style)
        s = Relationship(movie, "represented", node_s)
        graph.create(s)
```

运行后，数据库效果截图：

![kgclass03-2](C:\Users\kzcao\gitee\知识图谱实战\class03\kgclass03-2.png)

### 1.4 未完待续

后面查询部分待后2周展开。
